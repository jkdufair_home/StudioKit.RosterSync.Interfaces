﻿using System.Data.Entity;

namespace StudioKit.RosterSync.Interfaces
{
	public interface IRosterSyncDbContext<TGroup, TMembership, TGroupCourseSection, TTerm>
		where TGroup : class, IGroup<TMembership, TGroupCourseSection>, new()
		where TMembership : class, IMembership, new()
		where TGroupCourseSection : class, IGroupCourseSection, new()
		where TTerm : class, ITerm, new()
	{
		DbSet<TGroup> Groups { get; set; }
		DbSet<TMembership> GroupUserRoles { get; set; }
		DbSet<TGroupCourseSection> GroupCourseSections { get; set; }
		DbSet<TTerm> Terms { get; set; }
	}
}