﻿namespace StudioKit.RosterSync.Interfaces
{
	public interface IGroupCourseSection
	{
		int GroupId { get; set; }

		/// <summary>
		/// Id of the owner of the GroupCourseSection
		/// </summary>
		string UserId { get; set; }

		int ExternalId { get; set; }
	}
}