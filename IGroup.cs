﻿using System.Collections.Generic;

namespace StudioKit.RosterSync.Interfaces
{
	public interface IGroup<TMembership, TGroupCourseSection>
				where TMembership : IMembership
				where TGroupCourseSection : IGroupCourseSection
	{
		int Id { get; set; }
		string Name { get; set; }
		string TermCode { get; set; }
		bool IsDeleted { get; set; }
		ICollection<TMembership> UserRoles { get; set; }
		ICollection<TGroupCourseSection> CourseSections { get; set; }
	}
}